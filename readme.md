# Abaddon

A weapon addon for [Deathstrider](https://gitlab.com/accensi/deathstrider).

---

Hellish, rocket-grenade fed minigun.

Firing the gun will overheat it. Can install manual heat removal device.

---

See [credits.txt](./credits.txt) for attributions.
